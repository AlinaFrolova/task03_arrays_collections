package epam;

import java.util.ArrayList;
import java.util.Arrays;

public class Ship<T extends Droid> {
    protected ArrayList<T> droids;

    public Ship(T... droids) {
        this.droids = new ArrayList<T>(Arrays.asList(droids));
    }

    public void put(T droid) {
        droids.add(droid);
    }

    public T get(int index) {
        return droids.get(index);
    }


}
