package epam;

public class Droid implements Comparable<Droid> {
    public String name;

    public Droid(String name) {
        this.name = name;
    }

    public int compareTo(Droid other) {

        return name.compareTo(other.name);
    }
}
