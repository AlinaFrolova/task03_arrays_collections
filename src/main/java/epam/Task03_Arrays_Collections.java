package epam;

import java.util.ArrayList;
import java.util.List;

public class Task03_Arrays_Collections {

    public static void main(String[] args) {
        List<Integer> list = new ArrayList();
        list.add(1);
        String name = "Value";
        Object object = name;
        Integer value = (Integer) object;
        list.add(value);
    }
}
