package epam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class PriorityQueue<T extends Droid> {
    private ArrayList<T> droids;

    public PriorityQueue(T... droids) {
        this.droids = new ArrayList<T>(Arrays.asList(droids));
        Collections.sort(this.droids);
    }

    public void put(T droid) {
        droids.add(droid);
        Collections.sort(droids);
    }

    public T get(int index) {
        return droids.get(index);
    }


}
